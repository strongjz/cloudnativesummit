# Cloud Native Summit Online Website
The website is built with [Nanoc](https://nanoc.ws), hosted using [GitLab Pages]() and currently accessible using the domain name https://cloudnativesummit.online

## Website Directory Structure

The most important directories to be aware of are:

- `content` - All the content that is rendered on the final output are here
- `data` - JSON files storing event data like speakers and supporters
- `layouts` - template files used by all other pages
- `lib` - Ruby code used to generate certain content like redering JSON Data

## Making Changes

### Home Page (HTML/ERB)

File: `content/index.html`

Most content on the page are HTML, but it contains a few ERB content to render Featured Speakers and Supporters.

### Schedule (HTML)

File: `content/schedule.html`

It currently contains embed data for sched.com event management page

### Speakers (HTML/ERB)

File: `content/speakers.html`

Just like Index page it has an ERB to render all speakers from `data/talks.json` file.

### Support Us, Thanks, Code of COnduct Pages (Markdown)

Files: `content/*.md`

To ease editing since its mainly text, markdown is used for these pages.


### Adding Supporters

In the `data/event.json` file, inside in the `supporters` JSON array, you can add new supporters using the following format:

```json
    {
        "name": "GitLab", 
        "logo_url": "https://events.linuxfoundation.org/wp-content/uploads/gitlab-spn.svg",
        "website": "https://about.gitlab.com/", 
        "organizer": true
     }
```

Setting the `organizer` attribute to `true` will make the supported listed on the pages as an organizer, while `false` lists them as a supporter

### Adding Speakers

In `data/talks.json`, you can add a new speaker inside the `speakers` JSON array using the format:

```json
        {
            "slug": "richard",
            "name": "Richard ‘RichiH’ Hartmann",
            "bio": "",
            "affiliation": "Prometheus core team member and Grafana Labs Director",
            "image_url": "https://grafana.com/static/img/about/richard_hartmann.jpg",
            "featured": true,
            "social_media": 
                {
                    "twitter": "", 
                    "website": "", 
                    "medium": "", 
                    "linkedin": "", 
                    "others": ""
                }
        },
```

Setting `featured` to `true` lists the speaker on the homepage as a featured speaker while `false` lists them as spakers on the speakers page.

## How Sign Up is handled

Once a person registers, their data is appended to a Google Sheet using a Lambda function, which also sends an email to the email supplied:

```javascript
const { google } = require('googleapis')
const sheets = google.sheets('v4')
const RANGE = 'A2'
const SPREADSHEET_ID = process.env.sheet_id
const KEY = JSON.parse(process.env.safile)
const nodemailer = require('nodemailer');

const mailserver= {
    host: process.env.smtp_host,
    port: process.env.smtp_port,
    secure: process.env.smtp_secure,
    auth: {
      user: process.env.smtp_user,
      pass: process.env.smtp_password
    }
  };
  


const sendMail = async (mailserver, email, subject, email_body ) => {


  let mail = {
    from: process.env.smtp_email_from,
    to: email,
    subject: subject,
    html: email_body
  };

  // create a nodemailer transporter using smtp
  let transporter = nodemailer.createTransport(mailserver);

  // send mail using transporter
  let info = await transporter.sendMail(mail);

  console.log(`Preview: ${nodemailer.getTestMessageUrl(info)}`);
};


let jwtClient = new google.auth.JWT(
  KEY.client_email,
  null,
  KEY.private_key,
  [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/spreadsheets'
  ],
  null
)

exports.handler = (event, context, callback) => {
  jwtClient.authorize((err, tokens) => {
    if (err) {
      console.log(err)
      return callback(err)
    }
    let buff = new Buffer.from(event.body, 'base64');
    let buffData = buff.toString('ascii').split(",")

    //let timeNow  = Date('YYYY-MM-DD HH:mm:ss')
    //console.log(timeNow)
    //buffData.push(timeNow.replace(',', '.'))

    const body = {
      values: [JSON.parse(buffData)]
    }

    sheets.spreadsheets.values.append({
      auth: jwtClient,
      spreadsheetId: SPREADSHEET_ID,
      range: RANGE,
      valueInputOption: 'RAW',
      resource: body
    }, (err, result) => {
      if(err) {
        console.log(err)
        return callback(err)
      } else {
        sendMail(mailserver, buffData[1], process.env.email_subject, process.env.email_body ).catch(console.error);
        //console.log('%d cells appended.', result.updates.updatedCells);
        callback(null, {"statusCode": 200, "body": JSON.stringify(result)})
      }
    });
  });
};


```