---
title: Thanks
---

### You're the best 

We'd like this to use this space to thank all the folks contributing to the Virtual Cloud Native Summit even if they are not on the “stage” online. 

1. [Melissa Smolensky](https://twitter.com/melsmo) for sparking the idea
1. [Priyanka Sharma](https://twitter.com/pritianka) for leading the charge to put this event together
1. [Kaitlyn Barnard](https://twitter.com/kaitlyn_barnard) for jumping in to help with the event on day one
1. [Abubakar Siddiq Ango](https://twitter.com/sarki247) for building this website
1. [Matt Baldwin](https://twitter.com/baldwinmathew) for collaborating on the content from day one
1. [Kim McMahon](https://twitter.com/kamcmahon) for bringing in the CNCF as an organizer
1. [Puja Abbassi](https://twitter.com/puja108) for being the liaison between the event and the K8s Contributor Summit
1. [Chris Short](https://twitter.com/ChrisShort) for getting us started and requisitioning the Zoom account from CNCF
1. [Chris Kühl](https://twitter.com/rejektsio) and [Ariel Jatib](https://twitter.com/arieljatib) for their support
1. Header Photo by [Perry Grone](https://unsplash.com/@perrygrone?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash.
