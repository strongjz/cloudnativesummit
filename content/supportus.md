---
title: Support Us
---

### Support Us

There are two ways to support the event and be recognized for it.

1. **Supporters:** Any company who has a speaker represented will have a logo in the Supporter section. We thank you for making your employees available for this community cause :-). 
2. **Organizers:** It takes more planning effort to be listed as an organizer. If you are interested in getting involved, please email `abubakar @ gitlab . com` and request the privileges below:
    - Participation in event planning, including ability to drive content structure. We will include you in the planning conversations and meetings
    - Logo and link on the website listed as an organizer 
    - Mention in event intro slide
    - Tags in social promotion
    - Mention in email to registrants listing companies that are participating / helping
    - Organizers also have the ability to moderate break out sessions as long as the planning group agrees on the individual chosen being a good fit


